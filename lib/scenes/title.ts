import Phaser from "phaser";

export class TitleScene extends Phaser.Scene {
  config: any;
  timerEnd: any;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "titelScene";
    }

    if (!config.nextScene) {
      throw config.key + ": Missing next scene";
    }

    if (!config.titleText) {
      throw config.key + ": Missing text for title";
    }

    if (!config.time) {
      config.time = 5000;
    }

    super(config);
    this.config = config;
    Phaser.Scene.call(this, { key: "titleScene", active: false });
  }

  create() {
    console.log("Create Title scene with title " + this.config.titleText);
    this.add.text(25, 100, this.config.titleText, {
      fontFamily: "Arial",
      fontSize: 64,
      color: "red",
    });

    this.timerEnd = this.time.addEvent({
      delay: this.config.time,
      callback: this.onFinish,
      callbackScope: this,
    });
  }

  onFinish() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}
