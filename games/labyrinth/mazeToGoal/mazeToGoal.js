"use strict";
//Filväg till assets
const assetsPath = "../../../assets/";
import Phaser from 'phaser';

export class MazeToGoal extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MazeToGoal", active: false });
    this.gameOn = true;
    this.bonusUse1 = 0;
    this.bonusUse2 = 0;
    this.bonusScore = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
    this.timeText;
    this.gameOverText;
    this.my_buttons = [];
    this.step = 4;
  }
// Hittar väggarna till spelet
  preload() {
    this.load.image("maze", assetsPath + "labyrinth/straight-lines-maze.png");
  }
//Gör alla sprites, det vill säga grafiken
  create() {
    this.pen = this.make.graphics({ x: 0, y: 0, add: false });
    this.pen.fillStyle(0xff0000, 1.0);
    this.pen.fillCircle(10, 10, 10, 10);
    this.pen.generateTexture("player", 20, 20);
    this.player = this.add.image(380, 90, "player");

    this.pen = this.make.graphics({ x: 0, y: 0, add: false });
    this.pen.fillStyle(0x00ff00, 1.0);
    this.pen.fillRect(0, 0, 30, 30);
    this.pen.generateTexture("goal", 30, 30);
    this.goal = this.add.image(620, 270, "goal");

    this.maze = this.add.image(355, 300, "maze");

    this.pen = this.make.graphics({ x: 0, y: 0, add: false });
    this.pen.fillStyle(0xf1c232, 1.0);
    this.pen.fillRect(0, 0, 30, 30);
    this.pen.generateTexture("bonus", 20, 20);
    this.bonus1 = this.add.image(640, 30, "bonus");

    this.pen = this.make.graphics({ x: 0, y: 0, add: false });
    this.pen.fillStyle(0xf1c232, 1.0);
    this.pen.fillRect(0, 0, 30, 30);
    this.pen.generateTexture("bonus", 20, 20);
    this.bonus2 = this.add.image(380, 150, "bonus");


    this.scoreText = this.add.text(715, 25, "Time:", {
      fontSize: "24px",
      fill: "#000",
    });

    this.timeText = this.add.text(715, 45, "", {
      fontSize: "24px",
      fill: "#000",
    });
    this.gameOverText = this.add.text(160, 200, "", {
      fontSize: "100px",
      fill: "#ff0000",
    });
    this.gameScore = this.add.text(715, 65, "", {
      fontSize: "24px",
      fill: "#ff0000",
    });

    this.cursors = this.input.keyboard.createCursorKeys();
    this.deathTimer = this.time.addEvent({ delay: 1000, repeat: 80 });
  }

  update() {
    //Loggar spelarens position
    if (this.gameOn) {
      var lastStepX = this.player.x;
      var lastStepY = this.player.y;
//Ser till så att spelaren rör sig på rätt sätt
      if (this.cursors.left.isDown) {
        this.player.x -= this.step;
      } else if (this.cursors.right.isDown) {
        this.player.x += this.step;
      } else if (this.cursors.up.isDown) {
        this.player.y -= this.step;
      } else if (this.cursors.down.isDown) {
        this.player.y += this.step;
      }

     
      let color = this.textures.getPixel(this.player.x, this.player.y, "maze");
      this.timeText.setText(this.deathTimer.repeatCount);
      if (this.compareAlpha()) {
        this.player.x = lastStepX;
        this.player.y = lastStepY;
      }

      //Kollar ifall om du rör målet så vinner du
      if (
        Phaser.Geom.Intersects.RectangleToRectangle(
          this.player.getBounds(),
          this.goal.getBounds()
        )
      ) {
        this.scoreText.setText("Winner");
        this.timeText.setText(this.scoreMsg);
        this.gameScore.setText(
          this.bonusScore + this.deathTimer.repeatCount * 100
        );
        this.gameOn = false;
      }

      // Kontrollerar om du fick bonusen, om du fick bonusen får du 500 poäng till
      if (
        Phaser.Geom.Intersects.RectangleToRectangle(
          this.player.getBounds(),
          this.bonus1.getBounds()
        ) &&
        this.bonusUse1 < 1
      ) {
        this.bonusScore += 500;
        this.bonusUse1 += 1;
        this.bonus1.destroy();
      }

      if (
        Phaser.Geom.Intersects.RectangleToRectangle(
          this.player.getBounds(),
          this.bonus2.getBounds()
        ) &&
      this.bonusUse2 < 1
      ) {
        this.bonusScore += 500;
        this.bonusUse2 += 1;
        this.bonus2.destroy();
      }



 //Ser till så att när timern når 0 så är det gameover
      if (this.deathTimer.repeatCount == 0) {
        this.gameOn = false;
        this.scoreText.setText("");
        this.timeText.setText("");
        this.gameOverText.setText("GAME OVER");
      }
    }
  }

  compareAlpha() {
    return (
      this.compareAlphaOnPoint(this.player.getTopLeft()) ||
      this.compareAlphaOnPoint(this.player.getTopRight()) ||
      this.compareAlphaOnPoint(this.player.getBottomLeft()) ||
      this.compareAlphaOnPoint(this.player.getBottomRight())
    );
  }

  compareAlphaOnPoint(point) {
    return this.textures.getPixel(point.x, point.y, "maze").alpha === 255;
  }

}


