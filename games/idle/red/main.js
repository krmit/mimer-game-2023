"use strict";
const assetsPath = "../../../node_modules/@mimer/game-assets/";
import Phaser from "phaser";

export class IdleRedScene extends Phaser.Scene {
    constructor(config) {
        super(config);
        Phaser.Scene.call(this, { key: "IdleRedScene", active: false });
        this.player;
        this.pen;
        this.gameOn = true;
        this.score = 0;
        this.displayScore = 0;
        this.energy = 100;
        this.autoLevel = 1;
        this.clickLevel = 1;
        this.clickPower = 1;
        this.scoreMsg = "Score:  ";
        this.energyMsg = "Energy: ";
        this.autoMsg = "Auto lvl: ";
        this.clickPowerMsg = "Click power: ";
        this.upgradeCostMsg = "Upgrade cost: ";
        this.scoreText;
        this.energyText;
        this.autoText;
        this.clickPowerText;
        this.autoUpgradeText;
        this.clickPowerUpgradeText;
        this.goal;
        this.goalBG;
    }

    preload() {
        this.load.spritesheet(
            "buttons",
            "../../../assets/buttons/red-click-buttons-200x100-4.png",
            { frameWidth: 200, frameHeight: 100 }
        );
    }

    create() {
        var row = 100;
        const auto_button = this.add.image(550, row, "buttons", 0);
        const click_power_button = this.add.image(550, 400, "buttons", 3);
        this.scoreText = this.add.text(
            10,
            10,
            this.scoreMsg + this.displayScore,
            {
                fontSize: "32px",
                fill: "#FF0000",
            }
        );
        this.energyText = this.add.text(10, 40, this.energyMsg + this.energy, {
            fontSize: "32px",
            fill: "#FF0000",
        });
        this.autoText = this.add.text(10, 70, this.autoMsg + this.autoLevel, {
            fontSize: "32px",
            fill: "#FF0000",
        });
        this.clickPowerText = this.add.text(
            10,
            100,
            this.clickPowerMsg + this.clickPower + "x",
            {
                fontSize: "32px",
                fill: "#FF0000",
            }
        );
        this.autoUpgradeText = this.add.text(
            400,
            10,
            this.upgradeCostMsg + this.autoLevel,
            {
                fontSize: "32px",
                fill: "#FF0000",
            }
        );

        this.clickPowerUpgradeText = this.add.text(
            400,
            300,
            this.upgradeCostMsg + this.clickLevel * 20,
            {
                fontSize: "32px",
                fill: "#FF0000",
            }
        );

        this.pen = this.make.graphics({ x: 0, y: 0, add: false });
        this.pen.fillStyle(0xff0000, 1.0);
        this.pen.fillCircle(90, 90, 80);
        this.pen.generateTexture("RedButtonBG", 200, 400);
        this.goalBG = this.add.image(200, 450, "RedButtonBG");

        this.pen = this.make.graphics({ x: 0, y: 0, add: false });
        this.pen.lineStyle(15, 0xff0000, 1.0);
        this.pen.strokeCircle(90, 90, 80);
        this.pen.generateTexture("RedButton", 200, 400);
        this.goal = this.add.image(200, 450, "RedButton").setInteractive();
        this.goal.setInteractive();

        function addPointsHandler(points) {
            this.score += points;
            this.displayScore = Math.floor(this.score);
            if (this.goalBG.alpha < 1) {
                this.goalBG.alpha += 0.1;
            }

            if (this.displayScore % 10 === 0) {
                this.energy++;
            }
        }

        this.goal.on("pointerdown", () => {
            emitter.emit("addPoints", 1 * this.clickPower);
        });

        var emitter = new Phaser.Events.EventEmitter();
        emitter.on("addPoints", addPointsHandler, this);

        auto_button.setInteractive();
        auto_button.on("pointerdown", () => {
            if (this.energy >= this.autoLevel * 5) {
                this.energy -= this.autoLevel * 5;
                this.autoLevel++;
                this.time.addEvent({
                    delay: 200,
                    callback: function () {
                        emitter.emit("addPoints", 1);
                    },
                    callbackScope: this,
                    loop: true,
                });
            }
        });

        click_power_button.setInteractive();
        click_power_button.on("pointerdown", () => {
            if (this.energy >= this.clickLevel * 20) {
                this.energy -= this.clickLevel * 20;
                this.clickLevel++;
                this.clickPower = Math.round((this.clickPower + 0.1) * 10) / 10;
                this.time.addEvent({
                    delay: 200,
                    callback: function () {
                        emitter.emit("addPoints", 1);
                    },
                    callbackScope: this,
                    loop: true,
                });
            }
        });
    }

    update() {
        if (this.gameOn) {
            this.scoreText.setText(this.scoreMsg + this.displayScore);
            this.energyText.setText(this.energyMsg + this.energy);
            this.autoText.setText(this.autoMsg + this.autoLevel);
            this.clickPowerText.setText(
                this.clickPowerMsg + this.clickPower + "x"
            );
            this.autoUpgradeText.setText(
                this.upgradeCostMsg + this.autoLevel * 5
            );
            this.clickPowerUpgradeText.setText(
                this.upgradeCostMsg + this.clickLevel * 20
            );
            this.goalBG.alpha -= 0.001;
        }
    }
}
